//https://www.npmjs.com/package/cordova-plugin-admob-free
//You dont have to worry about caching banner ads, like video and full screen

var adMobBannerId = "ca-app-pub-7977392364071250/8371145322";
var adMobScreenId = "ca-app-pub-7977392364071250/5431025328";
var adMobVideoId = "ca-app-pub-7977392364071250/1320423717";
var adTesting = true;

function logInfo(data){
    document.getElementById('logs').innerHTML = document.getElementById('logs').innerHTML + data + "<br/>";
}

function clearLogs(){
    document.getElementById('logs').innerHTML = "";
}

function initilizeAds(){
    try{
        admob.banner.config({
            id: adMobBannerId,
            isTesting: adTesting,
            autoShow: false,
            overlap: true
        });
        
        admob.interstitial.config({
            id: adMobScreenId,
            isTesting: adTesting,
            autoShow: false,
        });
        
        admob.rewardvideo.config({
            id: adMobVideoId,
            isTesting: adTesting,
            autoShow: false,
        });
        
        admob.banner.prepare();
        admob.interstitial.prepare();
        admob.rewardvideo.prepare();
    }
    catch(error){
        logInfo("Error " + error);
    }

    logInfo(navigator.userAgent);
}

document.addEventListener('deviceready', initilizeAds);

/*
 * Banner Ads
 * Prepare gets Banner Ad ready, but still have to show it
 * Hide will just hide a prepared Banner ad, can still show it
 * Remove will remove banner ad, and before you can show it you need to prepare again
 * https://ratson.github.io/cordova-plugin-admob-free/variable/index.html#static-variable-banner
 *  Both Banner and Screen Ads
    admob.interstitial.events.LOAD
    admob.interstitial.events.LOAD_FAIL
    admob.interstitial.events.OPEN
    admob.interstitial.events.CLOSE
    admob.interstitial.events.EXIT_APP
 */

function showBannerAds(topBannerAds){
    logInfo("showBannerAds top = " + topBannerAds);
    admob.banner.config({
        id: adMobBannerId,
        isTesting: adTesting,
        autoShow: false,
        overlap: true,
        bannerAtTop: topBannerAds
    });
    admob.banner.prepare();
    //setTimeout(removeBannerAds, 5000);
}

function showTopBannerAds(){
    showBannerAds(true);
}

function showBottomBannerAds(){
    showBannerAds(false);
}

function hideBannerAds(){
    logInfo("hideBannerAds");
    admob.banner.hide();
}

function removeBannerAds(){
    logInfo("removeBannerAds");
    admob.banner.remove();
}

document.addEventListener('admob.banner.events.LOAD', function(event) {
    logInfo("banner LOAD");
    admob.banner.show();
});

document.addEventListener('admob.banner.events.LOAD_FAIL', function(event) {
    logInfo("banner fail");
});

document.addEventListener('admob.banner.events.CLOSE', function(event) {
    logInfo("banner CLOSE");
});

//Ad click exiting app now
document.addEventListener('admob.banner.events.EXIT_APP', function(event) {
    logInfo("banner EXIT_APP");
});

//Ad clicked and Opened
document.addEventListener('admob.banner.events.OPEN', function(event) {
    logInfo("banner OPEN");
});

/*
 * Screen Ads
 */
function prepareScreenAds(){
    logInfo("prepareScreenAds");
    admob.interstitial.prepare();
}

function showScreenAds(){
    logInfo("showScreenAds");
    admob.interstitial.show();
}
  
document.addEventListener('admob.interstitial.events.LOAD', function(event) {
    logInfo("interstitial load");
});

document.addEventListener('admob.interstitial.events.LOAD_FAIL', function(event) {
    logInfo("interstitial LOAD_FAIL");
});
    
document.addEventListener('admob.interstitial.events.CLOSE', function(event) {
    logInfo("interstitial prepare / CLOSE");
});

/*
 * Video Ads
 */
function prepareVideoAds(){
    logInfo("prepareVideoAds");
    admob.rewardvideo.prepare();
}

function showVideoAds(){
    logInfo("showVideoAds");
    admob.rewardvideo.show();
}

document.addEventListener('admob.rewardvideo.events.LOAD', function(event) {
    logInfo("rewardvideo LOAD");
});

document.addEventListener('admob.rewardvideo.events.LOAD_FAIL', function(event) {
    logInfo("rewardvideo LOAD_FAIL");
});
    
document.addEventListener('admob.rewardvideo.events.CLOSE', function(event) {
    logInfo("rewardvideo close");
});

document.addEventListener('admob.rewardvideo.events.REWARD', function(event) {
    logInfo("rewardvideo REWARD");
});

/*
admob.rewardvideo.events.LOAD
admob.rewardvideo.events.LOAD_FAIL
admob.rewardvideo.events.OPEN
admob.rewardvideo.events.CLOSE
admob.rewardvideo.events.EXIT_APP
admob.rewardvideo.events.START
admob.rewardvideo.events.REWARD
*/